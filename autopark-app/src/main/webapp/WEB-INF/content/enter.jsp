<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="/js/lib/bootstrap/v3_3_6/css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



    <link rel="stylesheet" type="text/css" href="/styles/enter.css" />
    <link rel="stylesheet" type="text/css" href="/styles/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


</head>
    <body>

    <%@ include file="tiles/header_v2.jsp" %>

    <div class="container">
        <br/>
        <%--<span class="title2">Вход в Ваш аккаунт приложения Автопарк</span><br><br>--%>

        <form action="<c:url value="/j_spring_security_check"></c:url>" method="post" name="loginForm" class="form-signin">
            <h2 class="form-signin-heading">Введите Ваш email и пароль</h2>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input name="username" id="inputEmail" placeholder="Ваш Email" class="form-control" type="text"  value="" autocomplete="on" required autofocus>

            <label for="inputPassword" class="sr-only">Password</label>
            <input name="password" class="form-control" placeholder="Ваш пароль" type="password" id="inputPassword" value="" required>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>

            <div class="checkbox">
                <label>
                    <a href="/lostpassword" class="blueLink">Не можете войти в свой аккаунт?</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    Нет еще аккаунта в Автопарке? <a href="/signup" class="blueLink">Регистрация здесь.</a>
                </label>
            </div>
        </form>


    </div> <!-- /container -->

    <hr>
    <%@ include file="tiles/footer.jsp" %>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/lib/bootstrap/v3_3_6/js/bootstrap.min.js"></script>

    </body>
</html>