<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="/js/lib/bootstrap/v3_3_6/css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="/js/date.js"></script>
    <link rel="stylesheet" type="text/css" href="/styles/profile.css" />
    <link rel="stylesheet" type="text/css" href="/styles/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<%@ include file="tiles/header_v2.jsp" %>

    <div class="container">
        <form class="form-profile">
            <h2>Личный кабинет</h2>
            <div class="form-group">
                <label for="ID">ID пользователя в системе:</label>
                <input name="id" type="text" readonly="readonly" class="form-control" id="ID" placeholder="ID" value="${command.id}">
            </div>
            <div class="form-group">
                <label for="NAME">Имя:</label>
                <input name="name" type="text"  class="form-control" id="NAME" placeholder="Имя" value="${command.name}">
            </div>

            <div class="form-group">
                <label for="EMAIL">Email:</label>
                <input name="email" type="text"  class="form-control" id="EMAIL" placeholder="Email" value="${command.email}">
            </div>

            <div class="form-group">
                <label for="PHONE">Телефон:</label>
                <input name="phone" type="text" class="form-control" id="PHONE" placeholder="Телефон" value="${command.phone}">
            </div>

            <div class="form-group">
                <label for="BIRTHDAY">Дата рождения:</label>
                <input name="birthday" type="text"  class="form-control datepicker" id="BIRTHDAY" placeholder="Дата рождения" autocomplete="off"
                       value='<fmt:formatDate value="${command.birthday}" pattern="dd.MM.yyyy" />'>
            </div>

            <div class="form-group">
                <label for="CRDATE">Дата создания:</label>
                <input name="creationDate" type="text" readonly="readonly" class="form-control" id="CRDATE" placeholder="Дата создания"
                       value='<fmt:formatDate value="${command.creationDate}" pattern="dd.MM.yyyy hh:mm:ss" />'>
            </div>


            <button type="submit" class="btn btn-default">Сохранить</button>
        </form>







    </div>


    <hr>
    <%@ include file="tiles/footer.jsp" %>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/lib/bootstrap/v3_3_6/js/bootstrap.min.js"></script>

</body>
</html>
