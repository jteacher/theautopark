package autopark.service.impl;

import autopark.service.IEmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

//aupark@yandex.ru
//aupark!123 - password

//aupark2@gmail.com
//aupark!123 - password

@Service
public class EmailSenderImpl implements IEmailSender {
    @Autowired
    private JavaMailSenderImpl sender;

    @Autowired
    private SimpleMailMessage messageTemplate;

    public void sendEmail(String to, String content, String subject) {
        SimpleMailMessage message = new SimpleMailMessage(messageTemplate);
        message.setText(content);
        message.setSubject(subject);
        message.setTo(to);
        try {
            sender.send(message);
        } catch (MailException e) {
            //log.error(e);
            e.printStackTrace();
        }
    }
}