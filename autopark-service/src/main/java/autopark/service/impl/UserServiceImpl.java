package autopark.service.impl;

import autopark.dao.IRoleUserDAO;
import autopark.dao.IUserDAO;
import autopark.domain.Role;
import autopark.domain.RoleUser;
import autopark.domain.User;
import autopark.dto.PrincipalUser;
import autopark.dto.RoleDTO;
import autopark.dto.UserDTO;
import autopark.service.IEmailSender;
import autopark.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("myUserService")
public class UserServiceImpl implements IUserService, UserDetailsService {
    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private IRoleUserDAO roleUserDAO;

    @Autowired
    private IEmailSender emailSender;

    @Autowired
    @Qualifier("encoder")
    private PasswordEncoder passwordEncoder;

    @Transactional
    public boolean createUser(UserDTO dto){
        User user = userDAO.getByEmail(dto.getEmail());
        if(user != null){
            return false;
        }
        user = new User();
        user.setName(dto.getName());
        user.setEmail(dto.getEmail());
        user.setPhone(dto.getPhone());
        user.setCreationDate(new Date());
        user.setBirthday(dto.getBirthday());


        user.setPassword(passwordEncoder.encode(dto.getPassword()));

        Long userId = userDAO.save(user);

        RoleUser roleUser = new RoleUser();
        roleUser.setUSER_ID(userId);
        roleUser.setROLE_ID(Role.ROLE_USER_ID);
        roleUserDAO.save(roleUser);

        //send email
        final User finalUser = user;
        new Thread(){
            public void run() {
                emailSender.sendEmail(finalUser.getEmail(), "Вы зарегистрировались в нашем Автопарке, с чем Вас и поздравляем.", "Успешная регистрация");
            }
        }.start();
        return true;
    }


    @Override
    public PrincipalUser loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userDAO.getByEmail(s);
        if(user != null){
            PrincipalUser springUser = new PrincipalUser();
            springUser.setEnabled(true);
            springUser.setPassword(user.getPassword());
            springUser.setUsername(user.getEmail());

            if(user.getAuthorities() != null){
                List<RoleDTO> roles = new ArrayList<RoleDTO>();
                for(Role role: user.getAuthorities()){
                    RoleDTO dto = new RoleDTO();
                    dto.setAuthority(role.getAuthority());
                    roles.add(dto);
                }
                springUser.setAuthorities(roles);
            }
            return springUser;
        }
        return null;
    }

    public UserDTO getUserByEmail(String email){
        User user = userDAO.getByEmail(email);
        if(user == null) {
            return null;
        }
        return assembleDTO(user);
    }

    private UserDTO assembleDTO(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setEmail(user.getEmail());
        userDTO.setCreationDate(user.getCreationDate());
        userDTO.setBirthday(user.getBirthday());
        userDTO.setPhone(user.getPhone());
        return userDTO;
    }

    public boolean processLostPassword(String ourServer, String email) {
        return false;
    }
}