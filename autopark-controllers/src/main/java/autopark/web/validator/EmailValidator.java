package autopark.web.validator;

import autopark.dto.UserDTO;
import autopark.utils.ApValidationUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by aro on 07.03.2016.
 */
@Component("emailValidator")
public class EmailValidator implements Validator {

    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "email.required");

    }

}
