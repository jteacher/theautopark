package autopark.web.auth;

import autopark.dto.PrincipalUser;
import autopark.dto.RoleDTO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Aspect
@Component
public class AuthenticationAspect {

    @Around("execution(public * *(..)) && @annotation(requiresAuthentication)")
    public Object go(ProceedingJoinPoint joinPoint, RequiresAuthentication requiresAuthentication) throws Throwable {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if(authentication == null || authentication.getPrincipal() == null){
                return "redirect:/enter";
            }
            String[] allowedRoles = requiresAuthentication.value();
            if(allowedRoles == null || allowedRoles.length == 0){
                return joinPoint.proceed();
            }

            PrincipalUser principalUser =  (PrincipalUser)authentication.getPrincipal();
            List<RoleDTO> roles = principalUser.getAuthorities();
            if(roles == null || roles.isEmpty()){
                return "redirect:/enter";
            }

            for(RoleDTO role: roles){
                for(String arole : allowedRoles){
                    if(arole.equals(role.getAuthority())){
                        return joinPoint.proceed();
                    }
                }
            }
        } catch (Exception e) {
            //log error
            e.printStackTrace();
        }
        return "redirect:/enter";
    }
}
