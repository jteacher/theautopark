package autopark.web.utils;

import autopark.dto.PrincipalUser;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by aro on 13.03.2016.
 */
public class SecurityUtils {
    public static final PrincipalUser getPrincipalUser(){
        return  (PrincipalUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
