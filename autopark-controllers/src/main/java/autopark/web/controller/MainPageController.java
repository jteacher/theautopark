package autopark.web.controller;

import autopark.dto.CarDTO;
import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

@Controller
public class MainPageController {

    @Autowired
    @Qualifier("carServiceHibernate")
    private ICarService carServiceHibernate;

    @Autowired
    @Qualifier("carServiceJdbc")
    private ICarService carServiceJdbc;


    @RequestMapping(value = {"/mainJdbc","/main","/"})
    public String handle(ModelMap modelMap) {
        return handleRequest(modelMap,carServiceJdbc);
    }

    @RequestMapping(value = "/mainHiber")
    public String handle2(ModelMap modelMap) {
        return handleRequest(modelMap,carServiceHibernate);
    }

    private String handleRequest(ModelMap modelMap, ICarService carService){
        Date start = new Date();
        List<CarDTO> list = carService.getCars();
        Date end = new Date();

        modelMap.put("millis",end.getTime()-start.getTime());

        modelMap.put("cars",list);
        modelMap.put("author",System.getProperty("user.name"));

        return "/WEB-INF/content/main.jsp";
    }






}



