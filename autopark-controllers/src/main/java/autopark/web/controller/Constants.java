package autopark.web.controller;

/**
 * Created by aro on 07.03.2016.
 */
public class Constants {
    public static final String COMMAND = "command";
    public static final String USER = "user";

    public static final String SIGNUP = "/WEB-INF/content/signup.jsp";
    public static final String ENTER  = "/WEB-INF/content/enter.jsp";


    private Constants() {
    }
}
